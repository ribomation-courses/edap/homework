#include <iostream>
#include <string>
#include <stdexcept>
#include "Calendar.hxx"

using namespace std;
using namespace std::string_literals;


Language ENG{
        {"Su"s,      "Mo"s,       "Tu"s,    "We"s,    "Th"s,  "Fr"s,   "Sa"s},
        {"January"s, "February"s, "March"s, "April"s, "May"s, "June"s, "July"s,
                "August"s, "September"s, "October"s, "November"s, "December"s}
};

Language SWE{
        {"Sö"s,      "Må"s,       "Ti"s,   "On"s,    "To"s,  "Fr"s,   "Lö"s},
        {"Januari"s, "Februari"s, "Mars"s, "April"s, "Maj"s, "Juni"s, "Juli"s,
                "Augusti"s, "September"s, "Oktober"s, "November"s, "December"s}
};

bool between(unsigned lb, unsigned x, unsigned ub) {
    return (lb <= x) && (x <= ub);
}


int main(int numArgs, char** args) {
    Date     current;
    unsigned year  = current.year();
    unsigned month = current.month();
    Language* lang = &ENG;
    unsigned startAt = lang->getWeekdayOffset();

    for (auto k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-m") month = static_cast<unsigned int>(stoi(args[++k]));
        else if (arg == "-y") year = static_cast<unsigned int>(stoi(args[++k]));
        else if (arg == "-a") startAt = static_cast<unsigned int>(stoi(args[++k]));
        else if (arg == "-s") lang = &SWE;
        else if (arg == "-e") lang = &ENG;
        else {
            cerr << "usage: " << args[0]
                 << " [-m <month>] [-y <year>] [-a <start-at-weekday>] [-s(wedish)] [-e(nglish)]\n";
            return 1;
        }
    }

    if (!between(1, month, 12))
        throw invalid_argument("month must be in [1, 12]. month=" + to_string(month));
    if (!between(0, startAt, 6))
        throw invalid_argument("start-at-weekday must be in [0, 6], where 0=Sunday. start-at=" + to_string(startAt));
    if (year < 1900)
        throw invalid_argument("year must be greater than 1900. year=" + to_string(year));

    lang->setWeekdayOffset(startAt);
    Calendar calendar{year, month};
    cout << calendar.render(*lang) << endl;

    return 0;
}

