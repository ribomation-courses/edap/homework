//
// Created by eguolyi on 2018-05-05.
//

#ifndef CLION_CALENDA_HXX
#define CLION_CALENDA_HXX

#include <vector>
#include <ostream>
#include <string>

using dateInfo = struct {
    unsigned day;
};

class Calenda {
    unsigned year;
    unsigned month;
    unsigned sizeOfMonth;
    unsigned weekDay;
    std::vector<dateInfo > monthDetail;
    void fillMonthDetail();

public:
    Calenda();
    Calenda(unsigned year, unsigned month);
    unsigned getWeekDay(unsigned y,unsigned m, unsigned d) const;
    void printByMonth (std::ostream& out, std::string language = "en") const;
    void printByYear () const;
    unsigned setDays();

};


#endif //CLION_CALENDA_HXX
